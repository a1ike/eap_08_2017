$(document).ready(function () {

  $('.er-home-welcome').slick({
    dots: true,
    arrows: true,
    infinite: true,
    autoplay: true,
    speed: 300,
    slidesToShow: 1
  });

  $('.er-news-cards').slick({
    dots: false,
    arrows: true,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        }
      }
    ]
  });

  $('.er-reviews').slick({
    dots: false,
    arrows: true,
    infinite: true,
    autoplay: true,
    speed: 300,
    slidesToShow: 1
  });

  $('.er-header__switcher').click(function () {
    $('.er-header__nav').slideToggle('fast', function () {
      // Animation complete.
    });
  });

  $('.er-top__place').delay(2000).slideToggle('fast');
  $('.er-top__placer > a').click(function () {
    $('.er-top__place').slideToggle('fast');
  });
  $('.er-top__place-close').click(function () {
    $('.er-top__place').slideToggle('fast');
  });

  $('.er-pre-table').click(function () {
    $(this).next().slideToggle('fast', function () {
      $(this).prev().find('.er-pre-table__arrow').toggleClass('er-pre-table__arrow_rotated');
    });
  });

});